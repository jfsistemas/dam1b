public class App {
    public static void main(String[] args) throws Exception {
        int numero = Integer.parseInt(args[0]);

        System.out.println("El número " + (numero - 1) + " es primo: " + esPrimo(numero - 1));
        System.out.println("El número " + numero + " es primo: " + esPrimo(numero));
        System.out.println("El número " + (numero + 1) + " es primo: " + esPrimo(numero + 1));
    }
    /**
     * Devuelve true si el número es primo o false si no lo es
     * @param numero
     * @return
     */
    public static boolean esPrimo(int numero) {
        boolean esPrimo = true;
        int divisor = 2;
        if (numero > 3){
            do {
                if (numero % divisor == 0) {
                    esPrimo = !esPrimo;
                }
                divisor++;
            } while (divisor <= (numero / 2) +1 && esPrimo);
        }
        return esPrimo;
    }

}
